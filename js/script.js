const carArray = [{
    "brand": "BMW",
    "models": [{
        "modelName": "2 Series",
        "versions": [{
            "versionName": "Active Tourer",
            "image": "https://cosystatic.bmwgroup.com/bmwweb/cosySec?COSY-EU-100-7331cqgv2Z7d%25i02uCaY3MuO2kOHUtWPfbYfvufM10tLhu1XzWVo7puMLWFmdkAj5DOPE%25x%25Z8XgY1nTNIowJ4HO3zkyXq%25sGM8snpq6v6ODubLz2aKqfkoOjmB2fJj5DOPMEagd%25kcWExHWpbl8FO2k3Hy2o24ByATQBrXpF2gplZ24riInMOscpF4HvmbX0KiIFJGHXXABHvIT9FqCO2JGvloImjgpT9GsLxdNUi591WmyGZI6ao5NF14ivAj0%25lsjV"
        }, {
            "versionName": "Cabriolet",
            "image": "https://cosystatic.bmwgroup.com/bmwweb/cosySec?COSY-EU-100-7331cqgv2Z7d%25i02uCaY3MuO2kOHUtWPfbYf7GIx10tLhu1XzWVo7puMLWFmdkAj5DOP7Wo%25Z8XgY1nTNIowJ4HO3zkyXq%25sGM8snpq6v6ODubLz2aKqfko2jmB2fJj5DOPMEagd%25kcWExHWpbl8FO2k3Hy2o24AAZTQBrXpFkCalZ24riIpZRscpF4HviNJ0KEFhzUG4fXx7IEL3huKrTRJeiHRq"
        }]
    }, {
        "modelName": "3 Series",
        "versions": [{
            "versionName": "Sedan",
            "image": "https://cosystatic.bmwgroup.com/bmwweb/cosySec?COSY-EU-100-7331cqgv2Z7d%25i02uCaY3MuO2kOHUtWPfbYf7GZg10tLhu1XzWVo7puMLWFmdkAj5DOPHNO%25Z8XgY1nTNIowJ4HO3zkyXq%25sGM8snpq6v6ODubLz2aKqfkoOjmB2fJj5DOPMEagd%25kcWExHWpbl8FO2k3Hy2o24B2MTQBrXpF2%25%25lZ24riInakscC4bVgvrt3RmFCoub%25cXJaHWpiah"
        }, {
            "versionName": "Gran Turismo",
            "image": "https://cosystatic.bmwgroup.com/bmwweb/cosySec?COSY-EU-100-7331cqgv2Z7d%25i02uCaY3MuO2kOHUtWPfbYf7GZg10tLhu1XzWVo7puMLWFmdkAj5DOPHLx%25Z8XgY1nTNIowJ4HO3zkyXq%25sGM8snpq6v6ODubLz2aKqfiSjjmB2fJj5DOPMEagd%25kcWExHWpbl8FO2k3Hy2o24ByfTQBrXpFhrGlZ24riInRsscpF4Hvi330KiIFJGHRAABNIqab9FSrW1vNyXq3B4lxTjHJxm"
        }]
    }, {
        "modelName": "4 Series",
        "versions": [{
            "versionName": "Coupé",
            "image": "https://cosystatic.bmwgroup.com/bmwweb/cosySec?COSY-EU-100-7331cqgv2Z7d%25i02uCaY3MuO2kOHUtWPfbYfyJxG10tLhu1XzWVo7puMLWFmdkAj5DOPMcA%25Z8XgY1nTNIowJ4HO3zkyXq%25sGM8snpq6v6ODubLz2aKqfkomjmB2fJj5DOPMEagd%25kcWExHWpbl8FO2k3Hy2o24ByfTQBrXpFhrGlZ24riInYHscpF4Hvi330KiIFJGHRAABNIqab9FSrW1vNyXq3B4lxTjHJxm"
        }, {
            "versionName": "Gran Coupé",
            "image": "https://cosystatic.bmwgroup.com/bmwweb/cosySec?COSY-EU-100-7331cqgv2Z7d%25i02uCaY3MuO2kOHUtWPfbYf7Gsd10tLhu1XzWVo7puMLWFmdkAj5DOPMoo%25Z8XgY1nTNIowJ4HO3zkyXq%25sGM8snpq6v6ODubLz2aKqfFo6jmB2fJj5DOPMEagd%25kcWExHWpbl8FO2k3Hy2o24ByfTQBrXpFhrGlZ24riIna1scpF4HvVYL0KiIFJGHxAABHvIT9JrrO2JGvloTWggpnG7xqLvQFjz9nE47rpI0eswTle1"
        }]
    }]
}, {
    "brand": "Audi",
    "models": [{
        "modelName": "A4",
        "versions": [{
            "versionName": "Limousine",
            "image": "https://mediaservice.audi.com/media/live/50900/fly1400x601n8/8w2/2018.png?wid=500"
        }, {
            "versionName": "Avant g-tron",
            "image": "https://mediaservice.audi.com/media/live/50900/fly1400x601n8/8w5al/2018.png?wid=500"
        }]
    }, {
        "modelName": "A5",
        "versions": [{
            "versionName": "Coupé",
            "image": "https://mediaservice.audi.com/media/live/50900/fly1400x601n8/f53/2018.png?wid=500"
        }, {
            "versionName": "Cabriolet",
            "image": "https://mediaservice.audi.com/media/live/50900/fly1400x601n8/f57/2018.png?wid=500"
        }]
    }, {
        "modelName": "A6",
        "versions": [{
            "versionName": "Limousine",
            "image": "https://mediaservice.audi.com/media/live/50900/fly1400x601n8/4a2/2019.png?wid=500"
        }, {
            "versionName": "Avant",
            "image": "https://mediaservice.audi.com/media/live/50900/fly1400x601n8/4a5/2019.png?wid=500"
        }]
    }]
}, {
    "brand": "Volkswagen",
    "models": [{
        "modelName": "Arteon",
        "versions": [{
            "versionName": "Elegance",
            "image": "https://www.volkswagen.nl/-/media/vwpkw/images/modellen/arteon/trimlinecards/arteon-elegance-trimlinecard_2048x802.ashx?w=1223&q=70&hash=43C2BDE6D61A936714F7CFCC8EBA01C0D050333E"
        }, {
            "versionName": "Business R",
            "image": "https://www.volkswagen.nl/-/media/vwpkw/images/modellen/fast-start-2018/trimline_arte_bur_door-5_rim-45s_lamp-8iu_color-6t6t_view-02.ashx?w=1223&q=70&hash=905B501302F42E11170B864CD9262B1E8D276472"
        }]
    }, {
        "modelName": "Golf",
        "versions": [{
            "versionName": "Trendline",
            "image": "https://www.volkswagen.nl/-/media/vwpkw/images/modellen/golf/trimlinecards/golf_trendline_cta_2048x802.ashx?w=1223&q=70&hash=8C2A08841ECA454B3A13B359E9AE54B8FE250CEE"
        }, {
            "versionName": "Highline",
            "image": "https://www.volkswagen.nl/-/media/vwpkw/images/modellen/golf/trimlinecards/golf_highline_cta_2048x802.ashx?w=1223&q=70&hash=F9338AAA66E1875ACBE5BA12991675C3DA36FA10"
        }]
    }, {
        "modelName": "Passat",
        "versions": [{
            "versionName": "Comfortline",
            "image": "https://www.volkswagen.nl/-/media/vwpkw/images/modellen/passat/trimlinecards/trimline_pass_comf_door-4_rim-f86_lamp-8id_color-4p4p_view-02.ashx?w=1223&q=70&hash=04FB94236EC6095894A9B9ACE048E16B92BA63F7"
        }, {
            "versionName": "Highline",
            "image": "https://www.volkswagen.nl/-/media/vwpkw/images/modellen/passat/trimlinecards/trimline_pass_high_door-4_rim-pjl_lamp-8id_color-0q0q_view-02.ashx?w=1223&q=70&hash=333EB08532956F6FE25E019D694478EDBC051B57"
        }]
    }]
}];

function createBrands(brandCreation) {
    let brandOptions = `<option disabled selected>Selecteer een merk</option>`;
    carArray.forEach(i => {
        brandOptions += `<option value="${i.brand}">${i.brand}</option>`;
    });
    brandCreation.innerHTML = brandOptions;
    document.getElementById('modelSelect').innerHTML = `<option disabled selected>Selecteer een model</option>`;
    document.getElementById('versionSelect').innerHTML = `<option disabled selected>Selecteer een uitvoering</option>`;
}

function changeSelectOptions(brandValue, modelValue, versionValue) {
    let modelOptions = '';
    let versionOptions = '';

    if (modelValue === 'Selecteer een model') {
        modelOptions = '<option disabled selected>Selecteer een model</option>';
    }
    if (versionValue === 'Selecteer een uitvoering') {
        versionOptions = '<option disabled selected>Selecteer een uitvoering</option>';
    }

    document.getElementById('sa-car-img-container').innerHTML = '';

    carArray.forEach(i => {
        if (i.brand === brandValue) {
            i.models.forEach(j => {
                if (j.modelName === modelValue) {
                    modelOptions += `<option selected value="${j.modelName}">${j.modelName}</option>`;
                    j.versions.forEach(k => {
                        if (k.versionName === versionValue) {
                            versionOptions += `<option selected value="${k.versionName}">${k.versionName}</option>`;
                        } else {
                            versionOptions += `<option value="${k.versionName}">${k.versionName}</option>`;
                        }
                        if (k.versionName === versionValue) {
                            document.getElementById('sa-car-img-container').innerHTML = 
                            `<p class="carName"><strong>${brandValue} ${modelValue} ${versionValue}</strong></p><img id="car" src="${k.image}">`;
                        }
                    });

                } else {
                    modelOptions += `<option value="${j.modelName}">${j.modelName}</option>`;
                }
            });
            document.getElementById('modelSelect').innerHTML = modelOptions;
            document.getElementById('versionSelect').innerHTML = versionOptions;
        }
    });
}